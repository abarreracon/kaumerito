"""Invoicing Forms"""

#Django
from django import forms

#Models
from invoicing.models import Company, Products

class CompanyForm(forms.ModelForm):

	class Meta:

		model = Company
		fields = ('company_name', 
			'address', 
			'phone_number', 
			'city',
			'state',
			'country',
		)

class ProductForm(forms.ModelForm):

	class Meta:

		company = forms.ModelChoiceField(queryset=Company.objects.filter(id=1))
		model = Products
		fields = ('company', 
			'product_name', 
			'sales_price', 
			'buy_price',
		)