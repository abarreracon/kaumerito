from django.apps import AppConfig


class InvoicingConfig(AppConfig):
    name = 'invoicing'
    varbose_name = 'Invoicing'

