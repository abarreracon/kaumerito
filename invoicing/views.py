#Django
from django.shortcuts import render
from django.views.generic import CreateView, DetailView, ListView
from django.urls import reverse_lazy

from django.http import HttpResponse

# Forms
from invoicing.forms import CompanyForm, ProductForm

#Models
from invoicing.models import Company, Products

def dashboard(request):
	return render(request, 'invoicing/dashboard.html')


class CreateCompanyView(CreateView):

	template_name = 'invoicing/create_company.html'
	form_class = CompanyForm
	success_url = reverse_lazy('invoicing:dashboard')

class ListCompanyView(ListView):

    template_name = 'invoicing/list_company.html'
    model = Company
    ordering = ('company_name',)
    paginate_by = 30
    context_object_name = 'companies'

class CreateProductView(CreateView):

	template_name = 'invoicing/create_product.html'
	form_class = ProductForm
	success_url = reverse_lazy('invoicing:products')

class ListProductsView(ListView):

    template_name = 'invoicing/list_products.html'
    model = Products
    ordering = ('product_name',)
    paginate_by = 30
    context_object_name = 'products'
