"""Invoicing URLs."""

#Django
from django.urls import path

#views

from invoicing import views

urlpatterns = [

	path(
		route='', 
		view=views.dashboard,
		name='dashboard'
	),
	path(
		route='create_company/', 
		view=views.CreateCompanyView.as_view(),
		name='create_company'
	),
	path(
    	route='company/', 
    	view=views.ListCompanyView.as_view(), 
    	name='companies'
    ),


    path(
    	route='create_product/', 
    	view=views.CreateProductView.as_view(), 
    	name='create_product'
    ),
    path(
    	route='products/', 
    	view=views.ListProductsView.as_view(), 
    	name='products'
    ),

]