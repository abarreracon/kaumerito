from django.db import models

# Create your models here.
class Company(models.Model):
	"""Company Data"""

	company_name = models.CharField(max_length=55)
	address = models.CharField(max_length=255, blank=True)
	phone_number = models.CharField(max_length=20, blank=True)
	city = models.CharField(max_length=255, blank=True)
	state = models.CharField(max_length=255, blank=True)
	country = models.CharField(max_length=255, blank=True)

	def __str__(self):
		return self.company_name

class Products(models.Model):
	"""Products of the Company"""

	company = models.ForeignKey(Company, on_delete=models.CASCADE)

	product_name = models.CharField(max_length=55)
	sales_price = models.CharField(max_length=55)
	buy_price = models.CharField(max_length=55)

	def __str__(self):
		"""Return name."""
		return self.product_name

